package pl.ishfid.mcwhiteboard.server;

import pl.ishfid.mcwhiteboard.server.chat.Chat;
import pl.ishfid.mcwhiteboard.server.chat.ChatManager;
import pl.ishfid.mcwhiteboard.server.commands.RoomCreate;
import pl.ishfid.mcwhiteboard.server.room.Room;
import pl.ishfid.mcwhiteboard.server.room.RoomManager;
import pl.ishfid.mcwhiteboard.server.user.User;
import pl.ishfid.mcwhiteboard.server.user.UserManager;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by ishfid on 3/17/16.
 */
public class CommandHandler {
    private Chat chat;
    private List<Room> roomList;
    private Room room;
    private List<User> userList;
    private User user;
    private BoardImage boardImage;

    private OutputStream outputStream;

    public CommandHandler(List<Room> roomList, BoardImage boardImage, Chat chat, Room room, List<User> userList, User user, OutputStream outputStream) {
        this.chat = chat;
        this.roomList = roomList;
        this.room = room;
        this.user = user;
        this.userList = userList;
        this.outputStream = outputStream;
        this.boardImage = boardImage;
    }

    public void handleCommand(String msg){
        String[] tokens = msg.split(" ",2);
        ChatManager chatManager = new ChatManager(chat, user, room);
        RoomManager roomManager = new RoomManager(roomList, room, outputStream);
        UserManager userManager = new UserManager(userList, user, outputStream);

        if(tokens[0].equals("CHAT")) {
            chatManager.manageChat(tokens[1]);
        }
        if(tokens[0].equals("ROOM")){
            roomManager.manageRoom(tokens[1]);
        }
        if(tokens[0].equals("USER")){
            userManager.manageUser(tokens[1]);
        }
        if(tokens[0].equals("CANVAS")){
            String[] tokens2 = tokens[1].split(" ",2);
            if(tokens2[0].equals("Update")){
                PrintStream out = new PrintStream(outputStream);
                out.println("SUCCESS ImageUpdated");
                boardImage.sendContent(tokens2[1]);
            }
        }
    }
}
