package pl.ishfid.mcwhiteboard.server.room;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by ishfid on 3/7/16.
 */
public class Room {
    private int roomId;
    private String roomName;
    private PrintStream out;

    public Room() {
        roomId = -1;
        roomName = "Dziwki";
    }

    public Room(String roomName) {
        this.roomName = roomName;
//        this.out = new PrintStream(outputStream);

    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomId() {
        return roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public void getRoomProperties(){
//        out.println(roomId + " " + roomName);
    }
}
