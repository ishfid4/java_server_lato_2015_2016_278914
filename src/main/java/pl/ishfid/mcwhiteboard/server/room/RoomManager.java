package pl.ishfid.mcwhiteboard.server.room;

import pl.ishfid.mcwhiteboard.server.commands.RoomCreate;
import pl.ishfid.mcwhiteboard.server.commands.RoomJoin;
import pl.ishfid.mcwhiteboard.server.commands.RoomList;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by ishfid on 3/18/16.
 */
public class RoomManager {
    private List<Room> roomsList;
    private Room room;
    private OutputStream outputStream;

    public RoomManager(List<Room> roomsList, Room room, OutputStream outputStream) {
        this.roomsList = roomsList;
        this.room = room;
        this.outputStream = outputStream;
    }

    public void manageRoom(String msg){
        String[] tokens = msg.split(" ",2);
        if(tokens[0].equals("RoomCreate")){
            RoomCreate roomCreate = new RoomCreate(roomsList, tokens[1]);
            roomCreate.execute();
            PrintStream out = new PrintStream(outputStream);
            out.println("SUCCESS ROOMADD");
        }
        if(tokens[0].equals("RoomList")){
            RoomList roomList = new RoomList(roomsList, outputStream);
            roomList.execute();
        }
        if (tokens[0].equals("RoomJoin")){
            RoomJoin roomJoin = new RoomJoin(roomsList, room, tokens[1], outputStream);
            roomJoin.execute();
        }
    }
}
