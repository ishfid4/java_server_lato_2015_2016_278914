package pl.ishfid.mcwhiteboard.server.chat;

import pl.ishfid.mcwhiteboard.server.commands.ChatJoin;
import pl.ishfid.mcwhiteboard.server.commands.ChatSend;
import pl.ishfid.mcwhiteboard.server.room.Room;
import pl.ishfid.mcwhiteboard.server.user.User;

/**
 * Created by ishfid on 3/18/16.
 */
public class ChatManager {
    private Chat chat;
    private User user;
    private Room room;

    public ChatManager(Chat chat, User user, Room room) {
        this.chat = chat;
        this.user = user;
        this.room = room;
    }

    public void manageChat(String msg){
        String[] tokens = msg.split(" ",2);
        if(tokens[0].equals("ChatSend")){
            ChatSend chatSend = new ChatSend(chat, tokens[1]);
            chatSend.execute();
        }
        if(tokens[0].equals("ChatJoin")){
            ChatJoin chatJoin = new ChatJoin(chat, user, room);
            chatJoin.execute();
        }
    }
}
