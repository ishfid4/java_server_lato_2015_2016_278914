package pl.ishfid.mcwhiteboard.server.chat;


import pl.ishfid.mcwhiteboard.server.room.Room;
import pl.ishfid.mcwhiteboard.server.Session;
import pl.ishfid.mcwhiteboard.server.user.User;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;


/**
 * Created by ishfid on 3/6/16.
 */
public class Chat {
    private PrintStream output;
    private List<Session> sessionList;
    private List<Room> roomList;
    private Session currentSession;
    private Room currentRoom;
    private User user;

    public Chat(List<Session> sessionList, List<Room> roomList, OutputStream outputStream) throws IOException {
        this.sessionList = sessionList;
        this.roomList = roomList;
        this.currentSession = sessionList.get(sessionList.size() - 1);
        this.currentRoom = currentSession.room;
        this.user = currentSession.user;
        this.output = new PrintStream(outputStream);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    private void sendNickname(String nickname) {
        output.print(nickname + ": ");
        output.flush();
    }

    private void sendMsg(String msg) {
        output.println("CHATMSG " + msg);
        output.flush();
    }

    public void sendContent(String msg) {
        for (Session session : sessionList) {
            if (session != currentSession) {
                if (session.room.getRoomId() == currentRoom.getRoomId()) {
                    //session.chat.sendNickname(user.getNickname());
                    session.chat.sendMsg(user.getNickname() + ": " + msg);
                }
            }
        }
    }
}
