package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.user.User;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by ishfid on 3/22/16.
 */
public class UserLogin implements Command{
    private List<User> userList;
    private User user;
    private PrintStream out;

    public UserLogin(List<User> userList, User user, OutputStream outputStream) {
        this.userList = userList;
        this.user = user;
        this.out = new PrintStream(outputStream);
    }

    @Override
    public void execute(){
        boolean loggedIn = false;
        for(User usr : userList){
            if(usr.getUsername().equals(user.getUsername())){
                if(usr.getPassword().equals(user.getPassword())){
                    user.setNickname(usr.getNickname());
                    out.println("SUCCESS LOGIN " + user.getNickname());
                    loggedIn = true;
                }else{
                    out.println("UserError Invalid Password");
                }
            }
        }
        if(!loggedIn)
            out.println("UserError Invalid Username or not registered user"); //Dunno if it wormks
    }
}
