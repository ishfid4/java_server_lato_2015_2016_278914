package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.chat.Chat;
import pl.ishfid.mcwhiteboard.server.room.Room;
import pl.ishfid.mcwhiteboard.server.user.User;

/**
 * Created by ishfid on 3/25/16.
 */
public class ChatJoin implements Command{
    private Chat chat;
    private User user;
    private Room room;

    public ChatJoin(Chat chat, User user, Room room) {
        this.chat = chat;
        this.user = user;
        this.room = room;
    }

    @Override
    public void execute(){
        chat.setUser(user);
        chat.setCurrentRoom(room);
    }

}
