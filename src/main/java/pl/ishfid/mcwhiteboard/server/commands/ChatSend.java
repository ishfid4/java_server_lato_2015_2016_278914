package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.chat.Chat;

/**
 * Created by ishfid on 3/18/16.
 */

public class ChatSend implements Command {
    private final String msg;
    private final Chat chat;

    public ChatSend(Chat chat, String msg) {
        this.chat = chat;
        this.msg = msg;
    }

    @Override
    public void execute() {
        String[] tokens = msg.split(" ", 2);
        chat.sendContent(tokens[1]);
    }
}