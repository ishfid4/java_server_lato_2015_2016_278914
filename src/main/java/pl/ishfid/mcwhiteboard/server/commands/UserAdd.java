package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.database.SQLiteJDBC;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by ishfid on 3/26/16.
 */
public class UserAdd implements Command {
    private String msg;
    private PrintStream out;

    public UserAdd(String msg, OutputStream outputStream) {
        this.msg = msg;
        this.out = new PrintStream(outputStream);
    }

    @Override
    public void execute(){
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        sqLiteJDBC.databaseAddUser(msg);
        out.println("SUCCESS SIGNUP");
    }
}
