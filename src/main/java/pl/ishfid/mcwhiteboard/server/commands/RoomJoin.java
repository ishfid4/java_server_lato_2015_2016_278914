package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.room.Room;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by ishfid on 3/19/16.
 */
public class RoomJoin implements Command{
    private List<Room> roomList;
    private Room room;
    private int roomId;
    private PrintStream out;

    public RoomJoin(List<Room> roomList, Room room, String roomId, OutputStream outputStream) {
        this.roomId = Integer.parseInt(roomId);
        this.room = room;
        this.roomList = roomList;
        this.out = new PrintStream(outputStream);
    }

    @Override
    public void execute(){
        //room = roomList.get(roomId - 1);
        room.setRoomName(roomList.get(roomId - 1).getRoomName());
        room.setRoomId(roomId);
        out.println("SUCCESS ROOMJOIN");
    }
}
