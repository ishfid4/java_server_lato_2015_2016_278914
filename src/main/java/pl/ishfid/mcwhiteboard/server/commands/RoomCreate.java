package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.room.Room;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by ishfid on 3/18/16.
 */

public class RoomCreate implements Command{
    private final String roomName;
    private List<Room> roomList;
    private Room room;
    private OutputStream outputStream;

    public RoomCreate(List<Room> roomList, String roomName) {
        this.roomList = roomList;
        this.roomName = roomName;
        this.outputStream = outputStream;
    }

    @Override
    public void execute(){
        room = new Room(roomName);
        roomList.add(room);
        roomList.get(roomList.size()-1).setRoomId(roomList.size());
//        room.setRoomId(roomList.size());
    }
}
