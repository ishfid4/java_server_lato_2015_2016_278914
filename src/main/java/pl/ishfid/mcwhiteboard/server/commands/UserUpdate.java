package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.database.SQLiteJDBC;
import pl.ishfid.mcwhiteboard.server.user.User;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by ishfid on 3/29/16.
 */
public class UserUpdate implements Command {
    private String msg;
    private User user;
    private PrintStream out;

    public UserUpdate(String msg, User user, OutputStream outputStream) {
        this.msg = msg;
        this.user = user;
        this.out = new PrintStream(outputStream);
    }

    @Override
    public void execute(){
        String[] tokens = msg.split(" ",2);
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        if(tokens[0].equals("EditPassword")){
            user.setPassword(tokens[1]);
            tokens[1] = "password " + tokens[1] + " " + user.getId();
            out.println("SUCCESS USERPASSCHANGE");
        }
        if(tokens[0].equals("EditNick")){
            user.setNickname(tokens[1]);
            tokens[1] = "nick " + tokens[1] + " " + user.getId();
            out.println("SUCCESS USERNICKCHANGE");
        }
        sqLiteJDBC.databaseUpdateUser(tokens[1]);
    }
}
