package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.room.Room;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by ishfid on 3/19/16.
 */
public class RoomList implements Command{
    private List<Room> roomList;
    private PrintStream out;

    public RoomList(List<Room> roomList, OutputStream outputStream) {
        this.roomList = roomList;
        this.out = new PrintStream(outputStream);
    }

    @Override
    public void execute(){
        for (Room room : roomList){
            out.println("ROOMGET " + room.getRoomId() + " " + room.getRoomName());
        }
    }
}
