package pl.ishfid.mcwhiteboard.server.commands;

import pl.ishfid.mcwhiteboard.server.database.SQLiteJDBC;

/**
 * Created by ishfid on 3/29/16.
 */
public class UserDelete implements Command {
    private String msg;

    public UserDelete(String msg) {
        this.msg = msg;
    }

    @Override
    public void execute(){
        SQLiteJDBC sqLiteJDBC = new SQLiteJDBC();
        sqLiteJDBC.databaseDeleteUser(msg);
    }
}
