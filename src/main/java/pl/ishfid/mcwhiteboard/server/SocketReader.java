package pl.ishfid.mcwhiteboard.server;

import pl.ishfid.mcwhiteboard.server.chat.Chat;
import pl.ishfid.mcwhiteboard.server.room.Room;
import pl.ishfid.mcwhiteboard.server.user.User;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ishfid on 3/5/16.
 */
public class SocketReader implements Runnable {
    private Scanner in;
    private PrintStream out;
    private Socket socket;
    private OutputStream outputStream;

    private CommandHandler commandHandler;
    private List<Session> sessionList;

    private User user;
    private List<User> userList;

    private Room room;
    private List<Room> roomList;

    private Chat chat;
    private BoardImage boardImage;

    public SocketReader(Socket socket, List<Session> sessionList, BoardImage boardImage, Chat chat, Room room, List<User> userList, User user, List<Room> roomList) throws IOException {
        this(socket.getInputStream(), socket.getOutputStream());
        this.socket = socket;
        this.outputStream = socket.getOutputStream();
        this.sessionList = sessionList;
        this.chat = chat;
        this.user = user;
        this.userList = userList;
        this.room = room;
        this.roomList = roomList;
        this.boardImage = boardImage;
    }

    public SocketReader(InputStream input, OutputStream output) {
        in = new Scanner(input);
        out = new PrintStream(output);
    }

    private void msg(String msg) {
        System.out.println("SRV: " + msg);
    }

    public void run() {
        msg("serving new connetions");
        this.commandHandler = new CommandHandler(roomList, boardImage, chat, room, userList, user, outputStream);

        while((!Thread.currentThread().isInterrupted()) && in.hasNextLine()) {
            String line = in.nextLine();
            msg(line);
            //Processing commands
            commandHandler.handleCommand(line);
        }
        try {
            out.close();
            socket.close();
        } catch (IOException e){
            e.printStackTrace();
        }
        msg("connetion closed");
    }
}
