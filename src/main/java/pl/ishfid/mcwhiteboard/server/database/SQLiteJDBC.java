package pl.ishfid.mcwhiteboard.server.database;

import pl.ishfid.mcwhiteboard.server.user.User;

import java.sql.*;
import java.util.List;

/**
 * Created by ishfid on 3/19/16.
 */
public class SQLiteJDBC {
    private User user;

    public SQLiteJDBC() { }

    public void databaseGetUsers(List<User> userList) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:server.sqlite");
            System.out.println("Opened Successfully");
            Statement stmt = connection.createStatement();
            try (ResultSet resultSet = stmt.executeQuery("SELECT * FROM users")) {
                //przeglądnięcie obiektu typu ResultSet element po elemencie
                while (resultSet.next()) {
                    //Wybranie kolumn w postaci Stringa
                    User user = new User(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
                    userList.add(user);
                    System.out.println(resultSet.getString(1) + " " + resultSet.getString(2) + " " + resultSet.getString(3) + " " + resultSet.getString(4));
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void databaseAddUser(String msg) {
        String[] tokens = msg.split(" ",4);
        Connection connection = null;
        Statement stmt = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:server.sqlite");
            connection.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = connection.createStatement();
            String sql = "INSERT INTO users (id,login,password,nick) " +
                    "VALUES (" + tokens[0] + ", '" + tokens[1] + "', '" +tokens[2] + "', '" +tokens[3]+"' );";
            stmt.executeUpdate(sql);

            stmt.close();
            connection.commit();
            connection.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Records created successfully");
    }


    public void databaseDeleteUser(String msg){
        Connection connection = null;
        Statement stmt = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:server.sqlite");
            connection.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = connection.createStatement();
            String sql = "DELETE from users where ID=" + msg + ";";
            stmt.executeUpdate(sql);
            connection.commit();

            stmt.close();
            connection.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }

    public void databaseUpdateUser(String msg){
        String[] tokens = msg.split(" ",3);

        Connection connection = null;
        Statement stmt = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:server.sqlite");
            connection.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = connection.createStatement();
            String sql = "UPDATE users set " + tokens[0] +" = '" + tokens[1] + "' where ID=" + tokens[2] +";";
            stmt.executeUpdate(sql);
            connection.commit();

            stmt.close();
            connection.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }
}
