package pl.ishfid.mcwhiteboard.server;

import pl.ishfid.mcwhiteboard.server.chat.Chat;
import pl.ishfid.mcwhiteboard.server.room.Room;
import pl.ishfid.mcwhiteboard.server.user.User;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

/**
 * Created by ishfid on 3/6/16.
 */
public class Session {
    public Chat chat;
    public Room room;
    public List<Room> roomList;
    public User user;
    public List<User> userList;
    public BoardImage boardImage;

    public Session(List<Session> sessions, List<Room> roomList, List<User> userList, Socket socket) throws IOException{
        sessions.add(this);
        this.roomList = roomList;
        this.userList = userList;
        //chat = new Chat(sessions, roomList, roomName, nickname, socket.getOutputStream());
        this.room = new Room();
        this.user = new User();
        this.boardImage = new BoardImage(sessions, roomList, socket.getOutputStream());
        this.chat = new Chat(sessions, roomList, socket.getOutputStream());

        SocketReader sReader = new SocketReader(socket, sessions, boardImage, chat, room, userList, user, roomList);
        new Thread(sReader).start();

    }
}
