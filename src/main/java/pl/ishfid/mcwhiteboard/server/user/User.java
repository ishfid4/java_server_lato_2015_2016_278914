package pl.ishfid.mcwhiteboard.server.user;

import pl.ishfid.mcwhiteboard.server.room.Room;

/**
 * Created by ishfid on 3/22/16.
 */
public class User {
    private int id;
    private String username;
    private String password;
    private String nickname;
    private Room room;

    public User(){ }

    public User(String id, String username, String password, String nickname) {
        this.id = Integer.parseInt(id);
        this.username = username;
        this.password = password;
        this.nickname = nickname;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getNickname() {
        return nickname;
    }

    public Room getRoomName() {
        return room;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setRoomName(Room room) {
        this.room = room;
    }
}
