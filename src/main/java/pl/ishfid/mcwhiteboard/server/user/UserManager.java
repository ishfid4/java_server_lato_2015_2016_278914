package pl.ishfid.mcwhiteboard.server.user;

import pl.ishfid.mcwhiteboard.server.commands.UserAdd;
import pl.ishfid.mcwhiteboard.server.commands.UserDelete;
import pl.ishfid.mcwhiteboard.server.commands.UserLogin;
import pl.ishfid.mcwhiteboard.server.commands.UserUpdate;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by ishfid on 3/22/16.
 */
public class UserManager {
    private User user;
    private List<User> userList;
    OutputStream outputStream;

    public UserManager(List<User> userList, User user, OutputStream outputStream) {
        this.userList = userList;
        this.user = user;
        this.outputStream = outputStream;
    }

    public void manageUser(String msg){
        String[] tokens = msg.split(" ",2);
        if(tokens[0].equals("UserLogin")){
            String[] userParameters = tokens[1].split(" ", 2);
            user.setUsername(userParameters[0]);
            user.setPassword(userParameters[1]);
            UserLogin userLogin = new UserLogin(userList, user, outputStream);
            userLogin.execute();
        }
        if(tokens[0].equals("UserAdd")){
            int id = userList.get(userList.size()-1).getId();
            id++;
            tokens[1] = id + " " + tokens[1];
            UserAdd userAdd = new UserAdd(tokens[1], outputStream);
            userAdd.execute();

            String[] userParams = tokens[1].split(" ",4);
            User user = new User(userParams[0],userParams[1],userParams[2],userParams[3]);
            userList.add(user);
        }
        if(tokens[0].equals("UserDelete")){
            UserDelete userDelete = new UserDelete(tokens[1]);
            userDelete.execute();
            userList.remove(Integer.parseInt(tokens[1]));
        }
        if(tokens[0].equals("UserUpdate")){
            UserUpdate userUpdate = new UserUpdate(tokens[1], user, outputStream);
            userUpdate.execute();
        }
    }
}
