package pl.ishfid.mcwhiteboard.server;

import pl.ishfid.mcwhiteboard.server.room.Room;
import pl.ishfid.mcwhiteboard.server.user.User;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by ishfid on 29.05.16.
 */
public class BoardImage {
    private PrintStream output;
    private List<Session> sessionList;
    private List<Room> roomList;
    private Session currentSession;
    private Room currentRoom;
    private User user;

    public BoardImage(List<Session> sessionList, List<Room> roomList, OutputStream outputStream) throws IOException {
        this.sessionList = sessionList;
        this.roomList = roomList;
        this.currentSession = sessionList.get(sessionList.size() - 1);
        this.currentRoom = currentSession.room;
        this.user = currentSession.user;
        this.output = new PrintStream(outputStream);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    private void sendMsg(String msg) {
        output.println("CANVAS Update " + msg);
        output.flush();
    }

    public void sendContent(String msg) {
        for (Session session : sessionList) {
                if (session.room.getRoomId() == currentRoom.getRoomId()) {
                    session.boardImage.sendMsg(msg);
                }
        }
    }
}
